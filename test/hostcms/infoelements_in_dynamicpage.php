<? // Страница «Сервис» #128 (Lubimka Mobile)

//////////////////////////
// Определение констант //
//////////////////////////

// RECAPTCHA_SITE_KEY                       // Открытый ключ для reCAPTCHA;
// GOOGLE_MAPS_KEY                          // Ключ для Google Maps
$iFAQInfosystemId = 22;                     // FAQ
$iFeedbackInfosystemId = 23;                // Обратная связь
$iCatalogsAndInstructionsInfosystemId = 24; // Каталоги и инструкции
$iServicesInfosystemId = 25;                // Сервисные центры
$iCertificatesInfosystemId = 26;            // Сертификация


?>
<? // FAQ
    $oInformationsystem = Core_Entity::factory('Informationsystem', $iFAQInfosystemId);
    $Informationsystem_Controller_Show = new Informationsystem_Controller_Show($oInformationsystem);
    $Informationsystem_Controller_Show
        ->xsl(Core_Entity::factory('Xsl')
            ->getByName('СервисFAQLubimkaMobile'))
        ->groupsMode('none')
        ->group(false)
        ->propertiesForGroups(array())
        ->groupsProperties(false)
        ->itemsPropertiesList(false)
        ->itemsProperties(false)
        ->itemsForbiddenTags(
            array(
                'informationsystem_id',
                'informationsystem_group_id',
                'shortcut_id',
                // 'name',
                // 'description',
                'active',
                'text',
                'image_large',
                'image_small',
                'image_large_width',
                'image_large_height',
                'image_small_width',
                'image_small_height',
                'sorting',
                'ip',
                'path',
                'indexing',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'siteuser_group_id',
                'showed',
                'user_id',
                'siteuser_id',
                'guid',
                'deleted',
                'url',
                'date',
                'datetime',
                'start_datetime',
                'end_datetime',
                'dir'
            )
        )
        ->limit(10)
        ->show();

    // Обратная связь
    $oInformationsystem = Core_Entity::factory('Informationsystem', $iFeedbackInfosystemId);
    $Informationsystem_Controller_Show = Core_Page::instance()->object;
    $Informationsystem_Controller_Show
        ->addEntity(Core::factory('Core_Xml_Entity')->name('recaptcha_site_key')->value(RECAPTCHA_SITE_KEY))
        ->xsl(Core_Entity::factory('Xsl')
            ->getByName('СервисОбратнаяСвязьLubimkaMobile'))
        ->groupsMode('none')
        ->group(false)
        ->propertiesForGroups(array())
        ->groupsProperties(false)
        ->itemsPropertiesList(true)
        ->itemsProperties(false)
        ->itemsForbiddenTags(
            array(
                'informationsystem_id',
                'informationsystem_group_id',
                'shortcut_id',
                // 'name',
                // 'description',
                'active',
                'text',
                'image_large',
                'image_small',
                'image_large_width',
                'image_large_height',
                'image_small_width',
                'image_small_height',
                'sorting',
                'ip',
                'path',
                'indexing',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'siteuser_group_id',
                'showed',
                'user_id',
                'siteuser_id',
                'guid',
                'deleted',
                'url',
                'date',
                'datetime',
                'start_datetime',
                'end_datetime',
                'dir'
            )
        )
        ->limit(10)
        ->show();

    // добавляем расширения файлов в свойства типа "файл"
    Core_Event::attach('property_value_file.onBeforeRedeclaredGetXml', array('Artatom_Observer_Property_Value_File', 'onBeforeRedeclaredGetXml'));
    // Каталоги и конструкции
    $oInformationsystem = Core_Entity::factory('Informationsystem', $iCatalogsAndInstructionsInfosystemId);
    $Informationsystem_Controller_Show = new Informationsystem_Controller_Show($oInformationsystem);
    $Informationsystem_Controller_Show
        ->xsl(Core_Entity::factory('Xsl')->getByName('СервисКаталогиИИнструкцииLubimkaMobile'))
        ->groupsMode('none')
        ->group(false)
        ->propertiesForGroups(array())
        ->groupsProperties(false)
        ->itemsPropertiesList(true)
        ->itemsProperties(true)
        ->itemsForbiddenTags(
            array(
                'informationsystem_id',
                'informationsystem_group_id',
                'shortcut_id',
                // 'name',
                'description',
                'active',
                'text',
                // 'image_large',
                // 'image_small',
                'image_large_width',
                'image_large_height',
                'image_small_width',
                'image_small_height',
                'sorting',
                'ip',
                'path',
                'indexing',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'siteuser_group_id',
                'showed',
                'user_id',
                'siteuser_id',
                'guid',
                'deleted',
                'url',
                'date',
                'datetime',
                'start_datetime',
                'end_datetime',
                // 'dir'
            )
        )
        ->limit(20)
        ->show();

    // Сервисные центры
    $oInformationsystem = Core_Entity::factory('Informationsystem', $iServicesInfosystemId);
    $Informationsystem_Controller_Show = new Informationsystem_Controller_Show($oInformationsystem);
    $Informationsystem_Controller_Show
        ->addEntity(Core::factory('Core_Xml_Entity')->name('google_maps_key')->value(GOOGLE_MAPS_KEY))
        ->xsl(Core_Entity::factory('Xsl')->getByName('СервисСервисныеЦентрыLubimkaMobile'))
        ->groupsMode('all')
        ->group(false)
        ->propertiesForGroups(array())
        ->groupsProperties(false)
        ->itemsPropertiesList(false)
        ->itemsProperties(true)
        ->itemsForbiddenTags(
            array(
                'informationsystem_id',
                // 'informationsystem_group_id',
                'shortcut_id',
                // 'name',
                'description',
                'active',
                'text',
                // 'image_large',
                // 'image_small',
                'image_large_width',
                'image_large_height',
                'image_small_width',
                'image_small_height',
                'sorting',
                'ip',
                'path',
                'indexing',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'siteuser_group_id',
                'showed',
                'user_id',
                'siteuser_id',
                'guid',
                'deleted',
                'url',
                'date',
                'datetime',
                'start_datetime',
                'end_datetime',
                // 'dir'
            )
        )
        ->limit(10)
        ->show();

    // Сертификация
    $oInformationsystem = Core_Entity::factory('Informationsystem', $iCertificatesInfosystemId);
    $Informationsystem_Controller_Show = new Informationsystem_Controller_Show($oInformationsystem);
    $Informationsystem_Controller_Show
        ->xsl(Core_Entity::factory('Xsl')->getByName('СервисСертификацияLubimkaMobile'))
        ->groupsMode('all')
        ->group(false)
        ->propertiesForGroups(array())
        ->groupsProperties(false)
        ->itemsPropertiesList(false)
        ->itemsProperties(false)
        ->itemsForbiddenTags(
            array(
                'informationsystem_id',
                // 'informationsystem_group_id',
                'shortcut_id',
                // 'name',
                'description',
                'active',
                'text',
                // 'image_large',
                // 'image_small',
                'image_large_width',
                'image_large_height',
                'image_small_width',
                'image_small_height',
                'sorting',
                'ip',
                'path',
                'indexing',
                'seo_title',
                'seo_description',
                'seo_keywords',
                'siteuser_group_id',
                'showed',
                'user_id',
                'siteuser_id',
                'guid',
                'deleted',
                'url',
                'date',
                'datetime',
                'start_datetime',
                'end_datetime',
                // 'dir'
            )
        )
        ->limit(1)
        ->show();

?>

