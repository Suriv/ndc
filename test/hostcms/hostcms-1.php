<?php
    $oShop = Core_Entity::factory('Shop', Core_Page::instance()->libParams['shopId']);
    $current_group = 0;
    $current_item = 0;
    if (is_object(Core_Page::instance()->object)
        && get_class(Core_Page::instance()->object) == 'My_Shop_Controller_Show')
    {
        $current_item = Core_Page::instance()->object->item;
        $current_group = Core_Page::instance()->object->group;
    }
?>
<div class="g-inner-page">
<?php
    // Вывод строки навигации
    $Structure_Controller_Breadcrumbs = new Structure_Controller_Breadcrumbs(
        Core_Entity::factory('Site', CURRENT_SITE)
    );

    $Structure_Controller_Breadcrumbs
        ->xsl(
            Core_Entity::factory('Xsl')->getByName('ХлебныеКрошкиProRiderz')
        )
        ->show();

    Core_Page::instance()->execute();

    if (Core::moduleIsActive('advertisement'))
    {
        if ($current_item == 0)
        {
            $Advertisement_Group_Controller_Show = new Advertisement_Group_Controller_Show(
                Core_Entity::factory('Advertisement_Group', 2)
            );
            $Advertisement_Group_Controller_Show
                ->limit(1)
                ->xsl(
                    Core_Entity::factory('Xsl')->getByName('ОтображениеБаннераProRiderz')
                )
                ->show();
        }
    }
?>

</div>

<?php
    if ($current_group >= 0 && $current_item == 0){
        /*Недавние заказы для разделов */
        $oCore_QueryBuilder_Select = Core_QueryBuilder::select(Core_QueryBuilder::expression('DISTINCT shop_items.*'))
           ->from('shop_orders')
           ->join('shop_order_items', 'shop_order_items.shop_order_id', '=', 'shop_orders.id')
           ->join('shop_items', 'shop_items.id', '=', 'shop_order_items.shop_item_id')
           ->where('shop_orders.shop_id', '=', $oShop->id)
           ->orderBy('shop_orders.datetime', 'DESC')
           ->limit(30);
        $aRows = $oCore_QueryBuilder_Select->execute()->asAssoc()->result();
        if (count($aRows) > 0)
        {
            $aShopItems = [];
            foreach ($aRows as $row)
            {
                if ($row['modification_id'] == 0)
                {
                    $aShopItems[] = $row['id'];
                }
                else
                {
                    $aShopItems[] = $row['modification_id'];
                }
            }

            $Shop_Controller_Show = new Shop_Controller_Show(
                Core_Entity::factory('Shop', $oShop->id)
            );
            $Shop_Controller_Show
                ->xsl(
                    Core_Entity::factory('Xsl')->getByName('МагазинНедавниеЗаказыНаГлавнойИВКаталогеProriderz')
                )
                ->itemsForbiddenTags(array('text'))
                ->groupsMode('all')
                ->group(FALSE)
                ->viewed(FALSE)
                ->comparing(FALSE)
                ->favorite(FALSE)
                ->itemsProperties(TRUE)
                ->modifications(TRUE)
                ->limit(30);

            $Shop_Controller_Show
                ->shopItems()
                ->queryBuilder()
                ->where('shop_items.id', 'IN', $aShopItems);

            $Shop_Controller_Show->show();
        }
    }
?>

<?php
    /*Новинки и популярное для всех разделов и карточки товара */
    if (Core::moduleIsActive('shop'))
    {
        $Shop_Controller_Show = new Shop_Controller_Show(
            Core_Entity::factory('Shop', $oShop->id)
        );

        $Shop_Controller_Show
            ->xsl(
                Core_Entity::factory('Xsl')->getByName('МагазинНовинкиВКаталогеProriderz')
            )
            ->itemsForbiddenTags(array('text'))
            ->groupsMode('all')
            ->group(FALSE)
            ->viewed(FALSE)
            ->comparing(FALSE)
            ->favorite(FALSE)
            ->itemsProperties(FALSE)
            ->modifications(TRUE)
            ->limit(15);

        $Shop_Controller_Show
            ->shopItems()
            ->queryBuilder()
            ->distinct()
            ->leftJoin(array('property_value_ints', 'show_zero'), 'show_zero.entity_id', '=', 'shop_items.id', array(
                array('AND' => array('show_zero.property_id', '=', 349)),
            ))
            ->leftJoin('shop_warehouse_items', 'shop_warehouse_items.shop_item_id', '=', 'shop_items.id')
            ->leftJoin(array('shop_items', 'modifications'), 'modifications.modification_id', '=', 'shop_items.id')
            ->leftJoin(array('shop_warehouse_items', 'modifications_shop_warehouse_items'), 'modifications_shop_warehouse_items.shop_item_id', '=', 'modifications.id')
            ->open()
                ->where('show_zero.value', '=', 1)
                ->setOr()
                ->where('shop_warehouse_items.count', '>', 0)
                ->setOr()
                ->where('modifications_shop_warehouse_items.count', '>', 0)
            ->close()
            ->where('shop_items.modification_id', '=', 0)
            ->where('shop_items.shortcut_id', '=', 0)
            ->clearOrderBy()
            ->orderBy('shop_items.showed', 'DESC'); /* популярное по количеству просмотров */

        $oShopItems_new = Core_Entity::factory('Shop_Item');
        $oShopItems_new->queryBuilder()
            ->distinct()
            ->join(array('property_value_ints', 'new'), 'new.entity_id', '=', 'shop_items.id', array(
                array('AND' => array('new.property_id', '=', 362)),
                array('AND' => array('new.value', '=', 1))
            ))
            ->leftJoin(array('property_value_ints', 'show_zero'), 'show_zero.entity_id', '=', 'shop_items.id', array(
                array('AND' => array('show_zero.property_id', '=', 349)),
            ))
            ->leftJoin('shop_warehouse_items', 'shop_warehouse_items.shop_item_id', '=', 'shop_items.id')
            ->leftJoin(array('shop_items', 'modifications'), 'modifications.modification_id', '=', 'shop_items.id')
            ->leftJoin(array('shop_warehouse_items', 'modifications_shop_warehouse_items'), 'modifications_shop_warehouse_items.shop_item_id', '=', 'modifications.id')
            ->open()
                ->where('show_zero.value', '=', 1)
                ->setOr()
                ->where('shop_warehouse_items.count', '>', 0)
                ->setOr()
                ->where('modifications_shop_warehouse_items.count', '>', 0)
            ->close()
            ->where('shop_items.active', '=', 1)
            ->where('shop_items.shop_id', '=', 3)
            ->where('shop_items.modification_id', '=', 0)
            ->where('shop_items.shortcut_id', '=', 0)
            ->limit(15);
        $aShopItems_New = $oShopItems_new->findAll();

        if (count($aShopItems_New) > 0)
        {
            $Shop_Controller_Show->addEntity(
                $oNewEntity = Core::factory('Core_Xml_Entity')
                    ->name('new_items')
            );
            foreach ($aShopItems_New as $oItem)
            {
                $oItem->showXmlModifications(TRUE);
                $oNewEntity->addEntity($oItem);
            }
        }
        $Shop_Controller_Show->show();
    }
?>

<?php
    if (($current_group>=0) && ($current_item == 0)){
        $oDocument = Core_Entity::factory('Document', 19);
?>
        <div class="g-seo-text">
            <div class="g-content">
                <div class="g-seo-text__in">
                    <div class="g-seo-text-title">
                        <span class="g-seo-text-title__txt Ff(din) Fw(b) Tt(u) g-les1"><?php echo htmlspecialchars($oDocument->name); ?></span>
                    </div>
                    <div class="g-seo-text__desc g-column_x2">
                        <div class="g-editor g-editor_sm g-editor_lh_big">
                            <?php $oDocument->Document_Versions->getCurrent()->execute(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
?>