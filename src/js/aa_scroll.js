'use strict'
const app = {
    scrolled: 0,
    newPosition: 0,
    interval: null,
    speed: 0,

    scrollTo: function(el) {
        let link = el.getAttribute('href').replace('#', ''),
            anchor = document.getElementById(link);

        let location = 0;
        if(anchor.offsetParent) {
            do {
                location += anchor.offsetTop;
                anchor = anchor.offsetParent;
            }
            while(anchor);
        }
        location = location >= 0 ? location : 0;

        this.animateScroll(location);
        return false;
    },

    animateScroll: function(pos) {
        if(document.documentElement.scrollTop <= 0)
            document.documentElement.scrollTop = 1;

        let element = (document.documentElement && document.documentElement.scrollTop) ? document.documentElement : document.body,
            start = element.scrollTop,
            change = pos - start - 70,
            currentTime = 0,
            increment = 20,
            duration = 600;

        const animateScroll = function() {
            currentTime += increment;
            element.scrollTop = Math.easeInOutQuad(currentTime, start, change, duration);
            if(currentTime < duration)
                setTimeout(animateScroll, increment);

        };
        animateScroll();
    },
};

Math.easeInOutQuad = function(t, b, c, d) {
    t /= d / 2;
    if(t < 1) return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
};

const links = document.querySelectorAll('.js-scrollto');
links.forEach(link => {
    link.addEventListener('click', function(event) {
        event.preventDefault();

        app.scrollTo(this);
    })
});
