// Swiper slider initial >>>>>>>>
;(function () {
    //  slider x4 staff
        var slider_x4_All = document.querySelectorAll('.g-slider-card--x4 .g-slider-card__in');
        if (slider_x4_All.length > 0) {
            [].forEach.call(slider_x4_All, function(slider){
                var prev = slider.querySelector('.g-slider-card-nav_prev');
                var next = slider.querySelector('.g-slider-card-nav_next');
                var mySwiper = new Swiper(slider, {
                    slidesPerView: 4,
                    slidesPerGroup: 3,
                    spaceBetween: 35,
                    watchSlidesVisibility: true,
                    touchEventsTarget: "wrapper",
                    simulateTouch: false,
                    // Disable preloading of all images
                    preloadImages: false,
                    // Enable lazy loading
                    lazy: {
                        loadPrevNext: true,
                    },
                    navigation:{
                        nextEl: next,
                        prevEl: prev
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        dynamicBullets: true,
                    },
                    watchOverflow: true,
                    breakpoints: {
                    // when window width is <= *px
                        576: {
                            slidesPerView: 1,
                            slidesPerGroup: 1,
                            spaceBetween: 15
                        },
                        767: {
                            slidesPerView: 2,
                            slidesPerGroup: 2,
                            spaceBetween: 15
                        },
                        992: {
                            slidesPerView: 3,
                            slidesPerGroup: 2,
                            spaceBetween: 20
                        },
                        1150: {
                            slidesPerView: 3,
                            slidesPerGroup: 2,
                            spaceBetween: 25
                        },
                        1440: {
                            slidesPerView: 4,
                            slidesPerGroup: 3,
                            spaceBetween: 25
                        }
                    }
                });
            });
        }

    //  slider x3 staff and office
        var slider_x3_All = document.querySelectorAll('.g-slider-card--x3 .g-slider-card__in');
        if (slider_x3_All.length > 0) {
            [].forEach.call(slider_x3_All, function(slider){
                var prev = slider.querySelector('.g-slider-card-nav_prev');
                var next = slider.querySelector('.g-slider-card-nav_next');
                var mySwiper = new Swiper(slider, {
                    slidesPerView: 3,
                    slidesPerGroup: 3,
                    spaceBetween: 35,
                    watchSlidesVisibility: true,
                    touchEventsTarget: "wrapper",
                    simulateTouch: false,
                    // Disable preloading of all images
                    preloadImages: false,
                    // Enable lazy loading
                    lazy: {
                        loadPrevNext: true,
                    },
                    navigation:{
                        nextEl: next,
                        prevEl: prev
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        dynamicBullets: true,
                    },
                    watchOverflow: true,
                    breakpoints: {
                    // when window width is <= *px
                        576: {
                            slidesPerView: 1,
                            slidesPerGroup: 1,
                            spaceBetween: 15
                        },
                        767: {
                            slidesPerView: 2,
                            slidesPerGroup: 2,
                            spaceBetween: 15
                        },
                        992: {
                            slidesPerView: 3,
                            slidesPerGroup: 2,
                            spaceBetween: 20
                        },
                        1150: {
                            slidesPerView: 3,
                            slidesPerGroup: 2,
                            spaceBetween: 25
                        },
                        1420: {
                            slidesPerView: 3,
                            slidesPerGroup: 3,
                            spaceBetween: 25
                        }
                    }
                });
            });
        }

    //  slider x2 offers
        var slider_x2_All = document.querySelectorAll('.g-slider-card--x2 .g-slider-card__in');
        if (slider_x2_All.length > 0) {
            [].forEach.call(slider_x2_All, function(slider){
                var prev = slider.querySelector('.g-slider-card-nav_prev');
                var next = slider.querySelector('.g-slider-card-nav_next');
                var mySwiper = new Swiper(slider, {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                    spaceBetween: 35,
                    watchSlidesVisibility: true,
                    touchEventsTarget: "wrapper",
                    simulateTouch: false,
                    // Disable preloading of all images
                    preloadImages: false,
                    // Enable lazy loading
                    lazy: {
                        loadPrevNext: true,
                    },
                    navigation:{
                        nextEl: next,
                        prevEl: prev
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        dynamicBullets: true,
                    },
                    watchOverflow: true,
                    breakpoints: {
                    // when window width is <= *px
                        599: {
                            slidesPerView: 1,
                            slidesPerGroup: 1,
                            spaceBetween: 15
                        },
                        767: {
                            slidesPerView: 1,
                            slidesPerGroup: 1,
                            spaceBetween: 20
                        },
                        1150: {
                            slidesPerView: 1,
                            slidesPerGroup: 1,
                            spaceBetween: 25
                        },
                        1280: {
                            slidesPerView: 2,
                            slidesPerGroup: 2,
                            spaceBetween: 25
                        }
                    }
                });
            });
        }

    // news slider
        var newsSlider = document.querySelector('.g-slider-card--news .g-slider-card__in');
        if (newsSlider) {
            var prev = newsSlider.querySelector('.g-slider-card-nav_prev');
            var next = newsSlider.querySelector('.g-slider-card-nav_next');
            var mySwiper = new Swiper(newsSlider, {
                slidesPerView: 3,
                slidesPerGroup: 2,
                spaceBetween: 35,
                watchSlidesVisibility: true,
                touchEventsTarget: "wrapper",
                simulateTouch: false,
                autoHeight: true,
                // Disable preloading of all images
                preloadImages: false,
                // Enable lazy loading
                lazy: {
                    loadPrevNext: true,
                },
                navigation:{
                    nextEl: next,
                    prevEl: prev
                },
                pagination: {
                    el: '.swiper-pagination',
                    dynamicBullets: true,
                },
                watchOverflow: true,
                breakpoints: {
                    // when window width is <= *px
                    599: {
                        slidesPerView: 1,
                        slidesPerGroup: 1,
                        spaceBetween: 15
                    },
                    767: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 20
                    },
                    1150: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 25
                    },
                    1280: {
                        slidesPerView: 3,
                        slidesPerGroup: 2,
                        spaceBetween: 25
                    }
                }
            });
        }

    // slider-tr see promo.html
        var sliderTR = document.querySelector('.slider-tr');
        if ( sliderTR ) {
            var sliderTRprev = sliderTR.querySelector('.slider-tr-nav_prev');
            var sliderTRnext = sliderTR.querySelector('.slider-tr-nav_next');
            new Swiper(sliderTR, {
                speed: 600,
                autoplay: {
                    delay: 10000,
                },
                spaceBetween: 45,
                slidesPerView: 1,
                // Disable preloading of all images
                // preloadImages: false,
                touchEventsTarget: "wrapper",
                simulateTouch: false,
                lazy: {
                    loadPrevNext: true
                },
                navigation:{
                    nextEl: sliderTRnext,
                    prevEl: sliderTRprev
                },
                autoHeight: true,
                watchOverflow: true
            });
        };

    // one slider
        var oneSliderAll = document.querySelectorAll('.slider-one');
        if (oneSliderAll.length > 0) {
            [].forEach.call(oneSliderAll, function(slider){
                const sliderIn = slider.querySelector('.slider-one__in');
                const prev = slider.querySelector('.slider-one-nav_prev');
                const next = slider.querySelector('.slider-one-nav_next');
                const pag = slider.querySelector('.slider-one__pag');
                const flagLoop = slider.querySelectorAll('.swiper-slide').length > 1;
                const oneSwiper = new Swiper( sliderIn , {
                    effect: "fade",
                    fadeEffect: {
                        crossFade: true
                    },
                    speed: 600,
                    autoplay: {
                        delay: 12000,
                    },
                    spaceBetween: 35,
                    slidesPerView: 1,
                    // Disable preloading of all images
                    preloadImages: false,
                    loop: flagLoop,
                    touchEventsTarget: "wrapper",
                    simulateTouch: false,
                    lazy: {
                        loadPrevNext: true
                    },
                    navigation:{
                        nextEl: next,
                        prevEl: prev
                    },
                    pagination: {
                        el: pag,
                        dynamicBullets: true,
                        clickable: true
                    },
                    watchOverflow: true
                });
            })
        }
}());
