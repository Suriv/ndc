"use strict";

const gulp     = require('gulp'),
connect        = require("gulp-connect"),
livereload     = require('gulp-livereload'),
notify         = require("gulp-notify"),
gulpif         = require('gulp-if'),
changed        = require('gulp-changed'),
plumber        = require('gulp-plumber'),
sourcemaps     = require("gulp-sourcemaps"),
gulpFilter     = require('gulp-filter'),
svgSprite      = require("gulp-svg-sprites"),
svg2png        = require('gulp-svg2png'),
imagemin       = require('gulp-imagemin'),
spritesmith    = require('gulp.spritesmith'),
nunjucksRender = require('gulp-nunjucks-render'),
beautify       = require('gulp-jsbeautifier'),
postcss        = require('gulp-postcss'),
postcssImport  = require('postcss-import'),
postcssNested  = require('postcss-nested'),
postcssCalc    = require('postcss-calc'),
presetEnv      = require('postcss-preset-env'),
clearfix       = require('postcss-clearfix'),
postcss_hexrgba   = require('postcss-hexrgba'),
discard_comments  = require('postcss-discard-comments'),
autoprefixer   = require('autoprefixer'),
pixrem         = require('pixrem'),
del            = require('del'),
argv           = require('yargs').argv;

// const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';
const isDevelopment = !argv.prod;

const path = {
    build: {},
    src: {},
    watch: {},
    clean: 'build'
};

const onError = function(err) {
    notify.onError({
    title:    "Error in " + err.plugin,
    message: err.message
    })(err);
    // beep(2);
    this.emit('end');
};

// Server connect
gulp.task('connect', function(cb) {
  connect.server({
    root: 'public',
    livereload: true
  });
  cb();
});

gulp.task('sprite_img', function () {
  var spriteData = gulp.src('src/sprites/sprite_img/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css'
  }));
  return spriteData.pipe(gulp.dest('public/images/sprite_img/'));
});

// Sprites svg and png
gulp.task('sprites_svg-png', function () {
  return gulp.src('src/sprites/sprites_svg-png/*.svg')
    .pipe(svgSprite({
        cssFile: "src/css/partials/base/_sprite.css"
    }))
    .pipe(gulp.dest("public/images/sprites/sprites_svg-png/")) // Write the sprite-sheet + CSS + Preview
    .pipe(gulpFilter("**/*.svg"))  // Filter out everything except the SVG file
    .pipe(svg2png())           // Create a PNG
    .pipe(gulp.dest("public/images/sprites_svg-png/"));
});

// Sprites svg and png
gulp.task('sprites_label', function () {
    return  gulp.src('src/sprites/sprites_label/*.svg')
            .pipe(svgSprite({
                mode: "symbols",
                svgId: "symbol-%f"
            }))
            .pipe(gulp.dest("public/images/sprites/sprites_label/"))
            .pipe(gulp.dest("src/html/sprites/sprites_label/"));
});

// imagemin
gulp.task('imgcompress', function () {
  return gulp.src('src/images/**/*')
    .pipe(imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({ plugins: [{ removeViewBox: false, removeTitle: true, removeDesc: true }] })
    ]))
    .pipe(gulp.dest('public/images/'));
});

gulp.task('css', function() {
    var plugins = [
        pixrem({ rootValue: 10 }),
        postcssCalc({preserve: false}),
        postcssImport,
        clearfix,
        postcssNested,
        postcss_hexrgba,
        presetEnv({
          stage: 3,
          preserve: false,
          importFrom: [
            'src/css/partials/abstracts/_variables.css',
            'src/css/partials/abstracts/_extend.css'
          ],
          features: {
            'custom-media-queries': true,
            'custom-properties': true
          }
        }),
        discard_comments
    ];
    return gulp.src('src/css/*.*css')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(changed('src/css/*.*css'))
        .pipe(gulpif(isDevelopment, sourcemaps.init({ loadMaps: true })))
        .pipe(postcss(plugins))
        .pipe(gulpif(isDevelopment, sourcemaps.write('./maps')))
        .pipe(gulp.dest('public/css/'))
        .pipe(connect.reload());
});

gulp.task('nunjucks', function() {
    return gulp.src(['src/html/*.html'])
        .pipe(plumber({ errorHandler: onError }))
        .pipe(changed('src/html/**/*.html'))
        .pipe(nunjucksRender({
            path: ['src/html/'] // String or Array
        }))
        .pipe(gulp.dest('public/'))
        .pipe(connect.reload());
});

// JS
gulp.task('js', function() {
  return gulp.src('src/js/**/*')
    .pipe(gulp.dest('public/js/'))
    .pipe(connect.reload());
});

// Fonts
gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('public/fonts/'))
    .pipe(connect.reload());
});

// Uploads
gulp.task('uploads', function() {
  return gulp.src('src/uploads/**/*')
    .pipe(gulp.dest('public/uploads/'))
    .pipe(connect.reload());
});

// Robots_txt
gulp.task('robots', function() {
  return gulp.src('src/robots.txt')
    .pipe(gulp.dest('public/'));
});

// Watch
gulp.task('watch', function (cb) {
      gulp.watch('src/css/**/*.*css', gulp.parallel('css'));
      gulp.watch('src/**/*.html', gulp.parallel('nunjucks'));
      gulp.watch('src/js/**/*', gulp.parallel('js'));
      gulp.watch('src/sprites/sprites_ico/*.svg', gulp.parallel('sprites_svg-png'));
      gulp.watch('src/sprites/sprites_label/*.svg', gulp.parallel('sprites_label'));
      gulp.watch('src/images/**/*', gulp.parallel('imgcompress'));
      gulp.watch('src/fonts/**/*', gulp.parallel('fonts'));
      cb();
  });

// start
gulp.task('start', gulp.parallel('robots', 'fonts', 'uploads', 'nunjucks', 'css', 'js', 'sprite_img', 'sprites_label', 'imgcompress'));
// del
gulp.task('del', gulp.series(function() { return del('public/'); }));
// Clean
gulp.task('clean', gulp.series(function() { return del('public/css/maps'); }) );
// Default
gulp.task('default', gulp.parallel('connect', 'watch'));
