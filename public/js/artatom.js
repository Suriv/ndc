// Loading Begin  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
(function() {
    // polyfill remove
    if (!Element.prototype.remove) {
        Element.prototype.remove = function remove() {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }
    // * polyfill remove


// from http://www.sberry.me/articles/javascript-event-throttling-debouncing
  function throttle(fn, delay) {
    var allowSample = true;

    return function(e) {
      if (allowSample) {
        allowSample = false;
        setTimeout(function() { allowSample = true; }, delay);
        fn(e);
      }
    };
  }

    // check support for css properties
    function supportCSS(prop) {
        var yes = false; // по умолчанию ставим ложь
        var prop1 = prop.replace(prop[0], prop[0].toUpperCase());
        if ('Moz' + prop1 in document.body.style) {
            yes = true; // если FF поддерживает, то правда
        }
        if ('webkit' + prop1 in document.body.style) {
            yes = true; // если Webkit поддерживает, то правда
        }
        if ('ms' + prop1 in document.body.style) {
            yes = true; // если IE поддерживает, то правда
        }
        if (prop in document.body.style) {
            yes = true; // если поддерживает по умолчанию, то правда
        }
        return yes; // возращаем ответ
    }
    if (!(supportCSS('pointerEvents'))) {
        document.documentElement.classList.add('no-pointer_events');
    }
    if (!(supportCSS('transform'))) {
        document.documentElement.classList.add('no-transform');
    }
    // /* check support for css properties

    // check support for placeholder
    if (!("placeholder" in document.createElement('input'))) {
        var _root = document.documentElement;
        var _head = document.head;
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.async = true;
        script.src = "https://cdnjs.cloudflare.com/ajax/libs/placeholders/4.0.1/placeholders.min.js";
        _head.appendChild(script);
        _root.classList.add('no-placeholder');
    }
    // /* check support for placeholder

    // determined scroll width
    window.SCROLLBAR_Width = 0; // glob var
    ;(function() {
        var scrollDiv = document.createElement('div'),
            body = document.body;
        scrollDiv.style = 'position:absolute;top:-9999px;width:50px;height:50px;overflow:scroll;';
        body.append(scrollDiv);
        SCROLLBAR_Width = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        scrollDiv.remove(scrollDiv);
        return window.SCROLLBAR_Width;
    })();
    // /* determined scroll width

    // navigation for mobile
    var navOpen = document.querySelectorAll(".js-nav-mob__open");
    if (navOpen.length > 0) {
        [].forEach.call(navOpen, function(item) {
            item.addEventListener("click", function() {
                if ((window.innerWidth - document.body.clientWidth) != 0) {
                    documentElement.style.paddingRight = SCROLLBAR_Width + "px";
                }
                if (documentElement.classList.contains("nav-open")) {
                    documentElement.style.paddingRight = "";
                    documentElement.classList.remove("is-lock");
                    documentElement.classList.remove("nav-open");
                } else {
                    documentElement.classList.add("is-lock");
                    documentElement.classList.add("nav-open");
                }
            });
        });
        var navClose = document.querySelectorAll(".js-nav-mob__close");
        if (navClose.length > 0) {
            [].forEach.call(navClose, function(item) {
                item.addEventListener("click", function() {
                    documentElement.style.paddingRight = "";
                    documentElement.classList.remove("is-lock");
                    documentElement.classList.remove("nav-open");
                });
            });
        }
    }
    // /* navigation for mobile

    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

    // MIT license
    (function() {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                       || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                  timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

    // dropdown menu is connected to the tab
    // $(".dropdown .nav-tabs-li__a").on("show.bs.tab", function() {
    //     var $this = $(this),
    //         name = $this.text();
    //     dropBtnTxt = $this.closest(".dropdown").find(".dropdown__btn-txt");
    //     dropBtnTxt.text(name);
    // });
    // /* dropdown menu is connected to the tab

    // Header fixed nav
        var WindowHeight = window.innerHeight || document.documentElement.clientHeight;
        var headerBottomOffset = document.querySelector(".header__bottom").offsetTop;
        if (headerBottomOffset == 0) {headerBottomOffset = 160;}
        // if(window.scrollY > WindowHeight) {
        // if(window.pageYOffset > WindowHeight) {
        //     document.querySelector('.js-to-top_btn').classList.add('active');
        // }
        window.onscroll = function() {
            var pageY = window.pageYOffset;
            if(pageY > headerBottomOffset) {
                documentElement.classList.add('is-header-fixed');
                document.querySelector(".header__bottom").classList.add('is-fixed');
            }
            if(pageY < headerBottomOffset) {
                documentElement.classList.remove('is-header-fixed');
                document.querySelector(".header__bottom").classList.remove('is-fixed');
            }
        };
    // header fixed nav

    // обертка для table
    (function(){
        var tables = document.querySelectorAll(".editor table");
        [].forEach.call(tables, function(table) {
            var div = document.createElement("div");
            div.className = "table__cont";
            table.parentNode.insertBefore(div, table);
            div.appendChild(table);
        });
    })(window, document);
    // * обертка для table

    (function() {
        // автосайз для textarea
        // autosize(document.querySelectorAll('.js_autosize'));

    })(window, document);

})(window, document);
// Loading End >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
